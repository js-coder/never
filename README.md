# Ни когда!

## Учёт версий произведения "Ни когда!"

- [Открыть последнюю версию произведения](https://gitlab.com/js-coder/never/blob/master/Ни%20когда!.txt) (актуальная версия 1.0.2.)

- [Посмотреть историю изменений](https://gitlab.com/js-coder/never/tags)

- [Инкрементальное сравнение версий](https://gitlab.com/js-coder/never/commits/master)

- [Создать запрос на изменение](https://gitlab.com/js-coder/never/merge_requests/new) (необходим [навык работы с контролем версий git](https://githowto.com/ru))
